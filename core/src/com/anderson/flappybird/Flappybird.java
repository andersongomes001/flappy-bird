package com.anderson.flappybird;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Circle;
import com.badlogic.gdx.math.Intersector;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.utils.viewport.StretchViewport;
import com.badlogic.gdx.utils.viewport.Viewport;

import java.util.Random;

public class Flappybird extends ApplicationAdapter {
	private SpriteBatch batch;
	private Texture[] passaro;
	private Texture fundo;
	private float variacao = 0;
	private Texture canoTopo;
	private Texture canoBaixo;

	private int larguraTela;
	private int alturaTela;
	private int velocidaQueda = 0;
	private int posicaoVertical = 0;

	private float posicaoMovimentoCanoHorizontal;
	private float espaco;
	private int barramenos = 0;

	private float deltaTime;
	private Random numeroRandom;
	private Texture gameover;
	private Texture chao;
	private Texture reiniciar;
	private float tamanhoCanoBaixo;
	private float tamanhoCanoAcima;

	private int estadoJogo = 0; //0-. nao iniciado, 1-> iniciado, 2->game over
	private BitmapFont fonte;
	private BitmapFont mensagem;
	private int pontuacao = 0;
	private Boolean marcouponto = false;

	private Circle circlePassaro;
	private Rectangle canosTopo;
	private Rectangle canosbaixo;

	private OrthographicCamera camera;
	private Viewport viewport;
	private final float VIRTUAL_WIDTH = 768;
	private final float VIRTUAL_HEIGHT = 1900;


	@Override
	public void create () {
		numeroRandom = new Random();

		circlePassaro = new Circle();


		batch = new SpriteBatch();
		passaro = new Texture[3];
		passaro[0] = new Texture("passaro1.png");
		passaro[1] = new Texture("passaro2.png");
		passaro[2] = new Texture("passaro3.png");
		fundo = new Texture("fundo.png");
		canoTopo = new Texture("cano_topo.png");
		canoBaixo = new Texture("cano_baixo.png");
		gameover = new Texture("game_over.png");
		reiniciar = new Texture("reiniciar.png");
		chao = new Texture("chao.png");

		camera = new OrthographicCamera();
		camera.position.set(VIRTUAL_WIDTH/2,VIRTUAL_HEIGHT/2, 0);
		viewport = new StretchViewport(VIRTUAL_WIDTH, VIRTUAL_HEIGHT, camera);
		larguraTela = (int)VIRTUAL_WIDTH;
		alturaTela  = (int)VIRTUAL_HEIGHT;
		fonte = new BitmapFont();
		mensagem = new BitmapFont();
		fonte.setColor(Color.WHITE);
		fonte.getData().setScale(6);

		mensagem.setColor(Color.WHITE);
		mensagem.getData().setScale(6);

		posicaoVertical = (alturaTela / 2);
		posicaoMovimentoCanoHorizontal = larguraTela + 100;
		espaco = 100;

	}

	@Override
	public void render () {

        Gdx.app.log("Altura", ""+VIRTUAL_HEIGHT);
        Gdx.app.log("Tamanho", ""+VIRTUAL_WIDTH);

		camera.update();

		// Limpar frames anteriores
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT | GL20.GL_DEPTH_BUFFER_BIT);


		deltaTime = Gdx.graphics.getDeltaTime();
		variacao += deltaTime * 10;
		if (variacao > 2) variacao = 0;

		if(estadoJogo == 0){
			tamanhoCanoAcima = canoTopo.getHeight();
			if(Gdx.input.isTouched()){
				estadoJogo = 1;
			}
		}else {
			velocidaQueda++;
			if (posicaoVertical > 0 || velocidaQueda < 0) posicaoVertical -= velocidaQueda;

			if(estadoJogo == 1){
				posicaoMovimentoCanoHorizontal -= deltaTime * 200;
				if (Gdx.input.justTouched()) velocidaQueda = -20;
				if (posicaoMovimentoCanoHorizontal < -canoTopo.getWidth()) {
					barramenos = 0;
					posicaoMovimentoCanoHorizontal = larguraTela;
					espaco = numeroRandom.nextInt(400) - 200;
					marcouponto = false;
				}

				if(posicaoMovimentoCanoHorizontal < 120 ){
					if(!marcouponto){
						pontuacao++;
						marcouponto = true;
					}

				}
			}else{
				if(Gdx.input.isTouched()){
					estadoJogo = 0;
					velocidaQueda = 0;
					pontuacao = 0;
					posicaoVertical = (alturaTela / 2);
					posicaoMovimentoCanoHorizontal = larguraTela + 100;
				}
			}

			if (espaco < 0) {
				tamanhoCanoBaixo = canoBaixo.getHeight() + Math.abs(espaco) + 200;
			} else {
				tamanhoCanoBaixo = canoBaixo.getHeight() - espaco + 200;
			}
			tamanhoCanoAcima = canoTopo.getHeight() + 200;


			Gdx.gl.glClearColor(1, 0, 0, 1);
			Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		}

		batch.setProjectionMatrix( camera.combined );

		batch.begin();

		batch.draw(fundo, 0,0, larguraTela, alturaTela);
		batch.draw(canoTopo, posicaoMovimentoCanoHorizontal, (alturaTela - (canoTopo.getHeight() + espaco) ), canoTopo.getWidth(), tamanhoCanoAcima);
		batch.draw(canoBaixo, posicaoMovimentoCanoHorizontal, 0, canoBaixo.getWidth(),tamanhoCanoBaixo );
		batch.draw(chao, 0, 0, larguraTela,40);
		batch.draw(passaro[(int)variacao], 120,posicaoVertical, 100, 100);

		fonte.draw(batch, String.valueOf(pontuacao), larguraTela / 2,alturaTela - 50);
		if(estadoJogo == 2){
			batch.draw(gameover,((larguraTela - gameover.getWidth()) / 2),(alturaTela / 2));
			batch.draw(reiniciar,((larguraTela - reiniciar.getWidth()) / 2),( (alturaTela / 2) - gameover.getHeight()) - 20  );
		}
		batch.end();

		circlePassaro.set( (120 + (passaro[(int)variacao].getWidth() / 2) ), (posicaoVertical + (passaro[(int)variacao].getHeight() / 2) ), (passaro[(int)variacao].getWidth()));
		canosbaixo = new Rectangle(posicaoMovimentoCanoHorizontal, 0, canoBaixo.getWidth(),tamanhoCanoBaixo );
		canosTopo = new Rectangle(posicaoMovimentoCanoHorizontal,(alturaTela - (canoTopo.getHeight() + espaco) ), canoTopo.getWidth(), tamanhoCanoAcima);
		if(Intersector.overlaps(circlePassaro, canosbaixo) || Intersector.overlaps(circlePassaro, canosTopo) || posicaoVertical <= 0 || posicaoVertical >= alturaTela){
			estadoJogo = 2;
		}
	}

	@Override
	public void dispose () {


	}
	@Override
	public void resize(int width, int height) {
		viewport.update(width, height);
	}
}
